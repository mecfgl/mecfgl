# -*- coding: utf-8 -*-
import numpy as np

usar_dados = input('Para usar dados do problema, inserir 0: ')

if usar_dados == '0':
    F1 = 720.0
    F2 = 480.0
    F3 = 800.0
    Fr = 1200.0
    alpha = 30.0*np.pi/180
    beta = 30.0*np.pi/180
else:
    F1 = float(input('Valor da F1: '))
    
    F2 = float(input('Valor da F2: '))
    
    F3 = float(input('Valor da F3: '))
    
    Fr = float(input('Valor da Forca Resultante: '))
    
    alpha = float(input('Angulo entre T e y (graus): '))
    alpha = alpha*np.pi/180
    
    beta = float(input('Angulo entre F3 e x (graus): '))
    beta = beta*np.pi/180


psi = float(input('Chute inicial em cos(teta): '))
psin = psi*1000

error = abs(psi-psin)/psi
while error > 1e-6:
    T = (F2*np.sqrt(1-psi**2)+F1-F3*np.cos(beta))/(np.sin(alpha))
    psin = (T*np.cos(alpha)+F3*np.sin(beta)-Fr)/(-F2)
    error = abs(psi-psin)/psi
    psin,psi = psi,psin
    
teta = np.arccos(psin)/np.pi*180

print('O valor de T é: ' + str(T) + ' N.\n')
print('O valor de teta é: ' + str(teta) + ' graus.\n')


Tmax = 1200.0
tetamax = 180.0
nx = 61
ny = 61
T, teta = np.meshgrid(np.linspace(0.0, Tmax, nx),
                         np.linspace(0.0, tetamax, ny))

Fr = T*np.cos(alpha)+F3*np.sin(beta)+F2*np.cos(teta*np.pi/180)

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

fig=plt.figure(figsize=(11,7),dpi=100)
ax=fig.gca(projection='3d')
ax.set_xlabel('T (N)')
ax.set_ylabel('teta (graus)')
ax.set_zlabel('Fr (N)')
surf1=ax.plot_surface(T,teta,Fr,cmap=cm.coolwarm,
               linewidth=1)
fig.colorbar(surf1, shrink=0.5, aspect=5)
plt.savefig('grafico.png')